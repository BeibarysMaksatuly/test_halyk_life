import Vue from 'vue'
import App from './App.vue'
import VueMask from 'v-mask'
import Vuelidate from 'vuelidate'

Vue.use(VueMask)
Vue.use(Vuelidate)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
